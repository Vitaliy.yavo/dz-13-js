/* Відповідь 1:
setTimeout() виконає код лише один раз через вказаний час і зупиниться.
setInterval() буде виконувати код регулярно з вказаним інтервалом до тих пір, поки його не зупинити: clearInterval().*/

/* Відповідь 2:
Якщо передати нульову затримку у функцію setTimeout() код не виконається миттєво, але буде доданий до черги завдань браузера.
Внутрішні механізми браузера та системного таймера все одно впливають на час виконання цього коду, тому важливо розуміти, що нульова затримка не означає миттєвого виконання.*/

/* Відповідь 3:
При виклику функції clearInterval(). Ця дія дозволяє припинити регулярне виконання коду, яке було налаштовано через setInterval().
Якщо не прописати, то це може призвести до зайвого використання ресурсів, непередбачених змін у вигляді програми та можливого дублювання операцій.*/

// Задача:

document.addEventListener("DOMContentLoaded", () => {
  const images = document.querySelectorAll(".image-to-show");
  const stopButton = document.createElement("button");
  stopButton.textContent = "Припинити";
  const resumeButton = document.createElement("button");
  resumeButton.textContent = "Відновити показ";
  let currentIndex = 0;
  let intervalId;

  function showImage(index) {
    images.forEach((image, i) => {
      if (i === index) {
        image.style.display = "block";
      } else {
        image.style.display = "none";
      }
    });
  }

  function startSlideshow() {
    intervalId = setInterval(() => {
      currentIndex = (currentIndex + 1) % images.length;
      showImage(currentIndex);
    }, 3000);
  }

  function stopSlideshow() {
    clearInterval(intervalId);
  }

  function resumeSlideshow() {
    startSlideshow();
  }

  stopButton.addEventListener("click", () => {
    stopSlideshow();
  });

  resumeButton.addEventListener("click", () => {
    resumeSlideshow();
  });

  showImage(currentIndex);
  startSlideshow();

  document.body.appendChild(stopButton);
  document.body.appendChild(resumeButton);
});
